<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "permohonan_dokumen".
 *
 * @property integer $id
 * @property integer $id_permohonan
 * @property integer $id_jenis_berkas
 * @property string $nama_file
 * @property string $created_at
 * @property string $updated_at
 *
 * @property JenisBerkas $jenisBerkas
 * @property Permohonan $permohonan
 */
class PermohonanDokumen extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $virtual_nama_file_upload;
    public $virtual_nama_file_download;
    
    public static function tableName()
    {
        return 'permohonan_dokumen';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_permohonan
            [['id_permohonan'], 'required'],
            [['id_permohonan'], 'integer'],
            [['id_permohonan'], 'exist', 'skipOnError' => true, 'targetClass' => Permohonan::className(), 'targetAttribute' => ['id_permohonan' => 'id']],

            //id_jenis_berkas
            [['id_jenis_berkas'], 'required'],
            [['id_jenis_berkas'], 'integer'],
            [['id_jenis_berkas'], 'exist', 'skipOnError' => true, 'targetClass' => JenisBerkas::className(), 'targetAttribute' => ['id_jenis_berkas' => 'id']],

            //nama_file
            [['nama_file'], 'required'],
            [['nama_file'], 'string', 'max' => 255],
            
            //virtual_nama_file_download
            [['virtual_nama_file_download'], 'safe'],
            
            //virtual_nama_file_upload
            [['virtual_nama_file_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_permohonan' => 'Id Permohonan',
            'id_jenis_berkas' => 'Id Jenis Berkas',
            'nama_file' => 'Nama File',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // $this->fileUpload();
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        /*$fileRoot = Yii::$app->params['configurations_file']['permohonan_dokumen-nama_file']['alias_upload_root'];
        $filePath = Yii::getAlias($fileRoot) . '/' . $this->id . '/' . $this->nama_file;
        if (is_file($filePath)) unlink($filePath);*/

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();

        /*if(!empty($this->nama_file)) {
            $downloadBaseUrl = Yii::$app->params['configurations_file']['permohonan_dokumen-nama_file']['alias_download_base_url'];
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_nama_file_download = $path . '/' . $this->nama_file;
        }*/
    }

    public function fileUpload()
    {
        if ($this->virtual_nama_file_upload && $this->validate()) {
            $uploadRoot = Yii::$app->params['configurations_file']['permohonan_dokumen-nama_file']['alias_upload_root'];
            $path = Yii::getAlias($uploadRoot) . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_nama_file_upload->saveAs($path . '/' . $this->nama_file);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisBerkas()
    {
        return $this->hasOne(JenisBerkas::className(), ['id' => 'id_jenis_berkas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermohonan()
    {
        return $this->hasOne(Permohonan::className(), ['id' => 'id_permohonan']);
    }
}
