<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class UserIdentity extends \technosmart\models\User
{

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}