<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "jenis_berkas".
 *
 * @property integer $id
 * @property integer $id_jenis_permintaan_dok
 * @property string $nama_berkas
 *
 * @property JenisPermintaanDok $jenisPermintaanDok
 * @property PermohonanDokumen[] $permohonanDokumens
 */
class JenisBerkas extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;

    public static function tableName()
    {
        return 'jenis_berkas';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_jenis_permintaan_dok
            [['id_jenis_permintaan_dok'], 'required'],
            [['id_jenis_permintaan_dok'], 'integer'],
            [['id_jenis_permintaan_dok'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPermintaanDok::className(), 'targetAttribute' => ['id_jenis_permintaan_dok' => 'id']],

            //nama_berkas
            [['nama_berkas'], 'required'],
            [['nama_berkas'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_jenis_permintaan_dok' => 'Id Jenis Permintaan Dok',
            'nama_berkas' => 'Nama Berkas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPermintaanDok()
    {
        return $this->hasOne(JenisPermintaanDok::className(), ['id' => 'id_jenis_permintaan_dok']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermohonanDokumens()
    {
        return $this->hasMany(PermohonanDokumen::className(), ['id_jenis_berkas' => 'id']);
    }
}
