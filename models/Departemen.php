<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "departemen".
 *
 * @property integer $id
 * @property string $departemen
 *
 * @property Mediator[] $mediators
 */
class Departemen extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'departemen';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //departemen
            [['departemen'], 'required'],
            [['departemen'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'departemen' => 'Departemen',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMediators()
    {
        return $this->hasMany(Mediator::className(), ['id_departemen' => 'id']);
    }
}
