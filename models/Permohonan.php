<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "permohonan".
 *
 * @property integer $id
 * @property string $nomor_permohonan
 * @property integer $id_pemohon
 * @property string $nik
 * @property string $nomor_hp
 * @property string $nama
 * @property string $email
 * @property string $alamat
 * @property string $keperluan
 * @property string $komentar
 * @property string $jenis_permohonan
 * @property integer $id_jenis_permintaan_dok
 * @property integer $id_petugas_loket
 * @property integer $id_mediator
 * @property string $nomor_hp_mediator
 * @property string $estimasi_hari
 * @property integer $verified_softcopy_by
 * @property integer $verified_hardcopy_by
 * @property integer $rejected_by
 * @property integer $accept_by
 * @property string $status_dokumen
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Pemohon $pemohon
 * @property JenisPermintaanDok $jenisPermintaanDok
 * @property Mediator $mediator
 * @property PermohonanDokumen[] $permohonanDokumens
 */
class Permohonan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'permohonan';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //nomor_permohonan
            [['nomor_permohonan'], 'required'],
            [['nomor_permohonan'], 'string', 'max' => 5],

            //id_pemohon
            [['id_pemohon'], 'integer'],
            [['id_pemohon'], 'exist', 'skipOnError' => true, 'targetClass' => Pemohon::className(), 'targetAttribute' => ['id_pemohon' => 'id']],

            //nik
            [['nik'], 'required'],
            [['nik'], 'string', 'max' => 16],

            //nomor_hp
            [['nomor_hp'], 'required'],
            [['nomor_hp'], 'string', 'max' => 16],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //email
            [['email'], 'string', 'max' => 128],

            //alamat
            [['alamat'], 'string'],

            //keperluan
            [['keperluan'], 'required'],
            [['keperluan'], 'string'],

            //komentar
            [['komentar'], 'required'],
            [['komentar'], 'string'],

            //jenis_permohonan
            [['jenis_permohonan'], 'required'],
            [['jenis_permohonan'], 'string'],

            //id_jenis_permintaan_dok
            [['id_jenis_permintaan_dok'], 'required', 'on' => ['dokumen', 'form-create']],
            [['id_jenis_permintaan_dok'], 'integer'],
            [['id_jenis_permintaan_dok'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPermintaanDok::className(), 'targetAttribute' => ['id_jenis_permintaan_dok' => 'id']],

            //id_petugas_loket
            [['id_petugas_loket'], 'integer'],

            //id_mediator
            [['id_mediator'], 'required', 'on' => ['konsultasi mediasi', 'form-create']],
            [['id_mediator'], 'integer'],
            [['id_mediator'], 'exist', 'skipOnError' => true, 'targetClass' => Mediator::className(), 'targetAttribute' => ['id_mediator' => 'id']],

            //nomor_hp_mediator
            [['nomor_hp_mediator'], 'string', 'max' => 16],

            //estimasi_hari
            [['estimasi_hari'], 'string'],

            //verified_softcopy_by
            [['verified_softcopy_by'], 'integer'],

            //verified_hardcopy_by
            [['verified_hardcopy_by'], 'integer'],

            //rejected_by
            [['rejected_by'], 'integer'],

            //accept_by
            [['accept_by'], 'integer'],

            //status_dokumen
            [['status_dokumen'], 'string'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor_permohonan' => 'Nomor Permohonan',
            'id_pemohon' => 'Pemohon',
            'nik' => 'Nik',
            'nomor_hp' => 'Nomor Hp',
            'nama' => 'Nama',
            'email' => 'Email',
            'alamat' => 'Alamat',
            'keperluan' => 'Keperluan',
            'komentar' => 'Komentar',
            'jenis_permohonan' => 'Jenis Permohonan',
            'id_jenis_permintaan_dok' => 'Jenis Permintaan Dokumen',
            'id_petugas_loket' => 'Petugas Loket',
            'id_mediator' => 'Mediator',
            'nomor_hp_mediator' => 'Nomor Hp Mediator',
            'estimasi_hari' => 'Estimasi Selesai',
            'verified_softcopy_by' => 'Softcopy Diverifikasi oleh',
            'verified_hardcopy_by' => 'Hardtcopy Diverifikasi oleh',
            'rejected_by' => 'Ditolak oleh',
            'accept_by' => 'Diselesaikan oleh',
            'status_dokumen' => 'Status Dokumen',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPemohon()
    {
        return $this->hasOne(Pemohon::className(), ['id' => 'id_pemohon']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPermintaanDok()
    {
        return $this->hasOne(JenisPermintaanDok::className(), ['id' => 'id_jenis_permintaan_dok']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMediator()
    {
        return $this->hasOne(Mediator::className(), ['id' => 'id_mediator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermohonanDokumens()
    {
        return $this->hasMany(PermohonanDokumen::className(), ['id_permohonan' => 'id']);
    }

    public static function smsNotifikasi($nomor_hp, $message)
    {
        // $this->smsNotifikasi('085297899409', 'novi cantik');
        // return true;
        
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.mainapi.net/smsnotification/1.0.0/messages",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"msisdn\"\r\n\r\n$nomor_hp\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n$message\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Bearer 40448fb4ca42d987606ddae2fd70a96c",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "postman-token: 73a75f80-cfd0-8293-a06b-2801104aa7d8"
            ),
            CURLOPT_CAINFO => 'C:\\xampp\\htdocs\\technosmart\\app_hackptsa_loket\\other/cacert.pem',
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            return true;
        }
    }
}
