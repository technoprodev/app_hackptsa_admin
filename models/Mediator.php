<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "mediator".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $id_departemen
 * @property string $nomor_hp
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Departemen $departemen
 * @property Permohonan[] $permohonans
 */
class Mediator extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'mediator';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],

            //id_departemen
            [['id_departemen'], 'required'],
            [['id_departemen'], 'integer'],
            [['id_departemen'], 'exist', 'skipOnError' => true, 'targetClass' => Departemen::className(), 'targetAttribute' => ['id_departemen' => 'id']],

            //nomor_hp
            [['nomor_hp'], 'required'],
            [['nomor_hp'], 'string', 'max' => 16],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'id_departemen' => 'Departemen',
            'nomor_hp' => 'Nomor Hp',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(Departemen::className(), ['id' => 'id_departemen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermohonans()
    {
        return $this->hasMany(Permohonan::className(), ['id_mediator' => 'id']);
    }
}
