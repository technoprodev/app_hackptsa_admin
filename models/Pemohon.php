<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "pemohon".
 *
 * @property integer $id
 * @property string $nik
 * @property string $nama
 * @property string $nomor_hp
 * @property string $email
 * @property string $alamat
 * @property integer $is_verified
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Permohonan[] $permohonans
 */
class Pemohon extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'pemohon';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'unique'],

            //nik
            [['nik'], 'required'],
            [['nik'], 'string', 'max' => 16],
            [['nik'], 'unique'],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 128],

            //nomor_hp
            [['nomor_hp'], 'required'],
            [['nomor_hp'], 'string', 'max' => 16],
            [['nomor_hp'], 'unique'],

            //email
            [['email'], 'string', 'max' => 128],
            [['email'], 'unique'],

            //alamat
            [['alamat'], 'string'],

            //is_verified
            [['is_verified'], 'required'],
            [['is_verified'], 'integer'],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'nomor_hp' => 'Nomor Hp',
            'email' => 'Email',
            'alamat' => 'Alamat',
            'is_verified' => 'Is Verified',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermohonans()
    {
        return $this->hasMany(Permohonan::className(), ['id_pemohon' => 'id']);
    }
}
