<?php
namespace app_hackptsa_admin\models;

use Yii;

/**
 * This is the model class for table "jenis_permintaan_dok".
 *
 * @property integer $id
 * @property string $jenis_permintaan_dok
 *
 * @property JenisBerkas[] $jenisBerkas
 * @property Permohonan[] $permohonans
 */
class JenisPermintaanDok extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenis_permintaan_dok';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //jenis_permintaan_dok
            [['jenis_permintaan_dok'], 'required'],
            [['jenis_permintaan_dok'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_permintaan_dok' => 'Jenis Permintaan Dok',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisBerkases()
    {
        return $this->hasMany(JenisBerkas::className(), ['id_jenis_permintaan_dok' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermohonans()
    {
        return $this->hasMany(Permohonan::className(), ['id_jenis_permintaan_dok' => 'id']);
    }
}
