<?php
$params['user.passwordResetTokenExpire'] = 3600;
$params['slider'] = false;

$config = [
    'id' => 'app_hackptsa_admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_hackptsa_admin\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_hackptsa_admin',
        ],
        'user' => [
            'identityClass' => 'app_hackptsa_admin\models\UserIdentity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_hackptsa_admin', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_hackptsa_admin',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;