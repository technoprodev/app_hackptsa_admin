<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['jenis_permintaan_dok']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['jenis_permintaan_dok'], ['class' => '']);
}

//
$jenisBerkases = [];
if (isset($model['jenis_berkas']))
    foreach ($model['jenis_berkas'] as $key => $jenisBerkas)
        $jenisBerkases[] = $jenisBerkas->attributes;

$this->registerJs(
    'vm.$data.jenis_permintaan_dok.jenisBerkases = vm.$data.jenis_permintaan_dok.jenisBerkases.concat(' . json_encode($jenisBerkases) . ');',
    // 'vm.$data.jenis_permintaan_dok.jenisBerkases = Object.assign({}, vm.$data.jenis_permintaan_dok.jenisBerkases, ' . json_encode($jenisBerkases) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['jenis_permintaan_dok']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['jenis_permintaan_dok'], ['class' => '']);
}

if (isset($model['jenis_berkas'])) foreach ($model['jenis_berkas'] as $key => $jenisBerkas) {
    if ($jenisBerkas->hasErrors()) {
        $error = true;
        $errorMessage .= Html::errorSummary($jenisBerkas, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['jenis_permintaan_dok'], 'jenis_permintaan_dok')->begin(); ?>
        <?= Html::activeLabel($model['jenis_permintaan_dok'], 'jenis_permintaan_dok', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['jenis_permintaan_dok'], 'jenis_permintaan_dok', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['jenis_permintaan_dok'], 'jenis_permintaan_dok', ['class' => 'help-block']); ?>
    <?= $form->field($model['jenis_permintaan_dok'], 'jenis_permintaan_dok')->end(); ?>

    <!-- 
    <?php if (isset($model['jenis_berkas'])) foreach ($model['jenis_berkas'] as $key => $value): ?>
        <?= $form->field($model['jenis_berkas'][$key], "[$key]nama_berkas")->begin(); ?>
            <?php /*Html::activeLabel($model['jenis_berkas'][$key], "[$key]nama_berkas", ['class' => 'control-label']);*/ ?>
            <?php /*Html::activeTextInput($model['jenis_berkas'][$key], "[$key]nama_berkas", ['class' => 'form-control', 'maxlength' => true]);*/ ?>
            <?php /*Html::error($model['jenis_berkas'][$key], "[$key]nama_berkas", ['class' => 'help-block']);*/ ?>
        <?= $form->field($model['jenis_berkas'][$key], "[$key]nama_berkas")->end(); ?>
    <?php endforeach; ?>
    -->

    <template v-if="typeof jenis_permintaan_dok.jenisBerkases == 'object'">
        <template v-for="(value, key, index) in jenis_permintaan_dok.jenisBerkases">
            <div v-show="!(value.id < 0)">
                <input type="hidden" v-bind:id="'jenisberkas-' + key + '-id'" v-bind:name="'JenisBerkas[' + key + '][id]'" class="form-control" type="text" v-model="jenis_permintaan_dok.jenisBerkases[key].id">
                <div v-bind:class="'form-group field-jenisberkas-' + key + '-nama_berkas'">
                    <label v-bind:for="'jenisberkas-' + key + '-nama_berkas'" class="control-label">Jenis Berkas</label>
                    <div class="input-group">
                        <input v-bind:id="'jenisberkas-' + key + '-nama_berkas'" v-bind:name="'JenisBerkas[' + key + '][nama_berkas]'" class="form-control" type="text" v-model="jenis_permintaan_dok.jenisBerkases[key].nama_berkas">
                        <div v-on:click="removeJenisBerkas(key)" class="input-group-addon bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                    </div>
                    <div class="help-block"></div>
                </div>
            </div>
        </template>
    </template>

    <a v-on:click="addJenisBerkas" class="btn btn-default">Add Jenis Berkas</a>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['jenis_permintaan_dok']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>