<?php

use app_hackptsa_admin\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\yii\widgets\Alert;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "succes");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <!-- START @WRAPPER -->
        <section id="wrapper">
            <!-- START @HEADER -->
            <header id="header" class="bg-dark">
                <div class="container">
                    <a href="home.html" class="margin-y-10 margin-right-10 pull-left"><img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="25"></a>
                    <ul class="menu-x menu-sm menu-space-lg submenu-xs submenu-space-xs menu-hover-bg-gray submenu-bg-gray submenu-hover-bg-dark pull-left hidden-sm-less">
                        <li><a>Announcement</a></li>
                        <li><a>Article</a></li>
                    </ul>

                    <?php
                        $cache = Yii::$app->cache;
                        $key = 'menu-starter';
                        if ($cache->exists($key)) {
                            $cacheData = $cache->get($key);
                        } else {
                            $cacheData = [
                                'starter' => technosmart\models\Menu::find()->where(['code' => 'starter'])->asArray()->all(),
                            ];
                            $cache->set($key, $cacheData);
                        }
                        // Yii::warning($cacheData['starter']);
                    ?>

                    <?php
                        $menuStarter = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['starter'], null, true),
                            'options' => [
                                'class' => 'menu-x menu-sm menu-space-lg submenu-xs submenu-space-xs menu-active-bg-gray submenu-active-bg-dark menu-hover-bg-gray submenu-bg-gray submenu-active-bg-gray submenu-hover-bg-dark pull-right hidden-sm-less',
                                'id' => 'menu-x',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php if ($menuStarter): ?>
                        <?= $menuStarter ?>
                    <?php endif; ?>

                    <div class="pull-right visible-sm-less padding-y-15 fs-16">
                        <i class="fa fa-navicon"></i>
                    </div>
                </div>
                <!-- <div class="fs-18 pull-left margin-left-20" style="width: 230px;">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="underline-none">
                        <span class="text-dark-azure f-italic"><?= Yii::$app->params['app.name'] ?></span>
                    </a>
                </div> -->
            </header>
            <!-- /END @HEADER -->

            <!-- START @BODY -->
            <section id="body">
                <!-- START @SIDEBAR LEFT -->
                <!-- <aside id="sidebar-left" class="border-right">
                </aside> -->
                <!-- /END @SIDEBAR LEFT -->

                <!-- START @PAGE WRAPPER -->
                <section id="page-wrapper">
                    <!-- START @PAGE SLIDER -->
                    <?php if (Yii::$app->params['slider']) : ?>
                    <section id="page-slider" class="has-bg-img">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpg" class="bg-img" style="width: 100%; height: auto;">
                        <!-- <img src="../technoart/asset/img/10-azure.png" class="bg-img" style="width: 100%; height: auto;"> -->
                        <div class="container padding-y-100 m-padding-y-60 text-lightest">
                            <div class="margin-bottom-50">
                                <span class="padding-20 fs-40 m-fs-26 darker-50"><?= Yii::$app->params['app.name'] ?></span>
                            </div>
                            <div>
                                <span class="padding-20 fs-20 m-fs-13 darker-20"><?= Yii::$app->params['app.description'] ?></span>
                            </div>
                        </div>
                    </section>
                    <?php endif; ?>
                    <!-- /END @PAGE SLIDER -->

                    <!-- START @PAGE HEADER -->
                    <!-- <section id="page-header" class="border-bottom border-lighter">
                        <div class="fs-20"><?= Html::decode($this->title) ?></div>
                    </section> -->
                    <!-- /END @PAGE HEADER -->

                    <!-- START @PAGE BODY -->
                    <section id="page-body" class="padding-0">
                        <?= $content ?>
                    </section>
                    <!-- /END @PAGE BODY -->
                </section>
                <!-- /END @PAGE WRAPPER -->

                <!-- START @SIDEBAR RIGHT -->
                <!-- <aside id="sidebar-right" class="border-left">
                </aside> -->
                <!-- /END @SIDEBAR RIGHT -->
            </section>
            <!-- /END @BODY -->

            <!-- START @FOOTER -->
            <footer id="footer" class="padding-0">
                <!-- <div class="container text-center fs-18"><?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?></div> -->
                <div class="bg-darker">
                    <div class="container">
                        <div class="box box-space-lg box-break-sm text-center-sm-less">
                            <div class="box-3 m-margin-bottom-30 text-center">
                                <a href="home.html"><img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="100"></a>
                            </div>
                            <div class="box-3 m-margin-bottom-30">
                                <div class="text-gray">
                                    Learn about <?= Yii::$app->params['app.name'] ?>
                                </div>
                                <div class="margin-top-10 fs-14">
                                    About Us
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Company Profile
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Contact Sales
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Privacy Policy
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Contact Us
                                </div>
                            </div>
                            <div class="box-3 m-margin-bottom-30">
                                <div class="text-gray">
                                    <?= Yii::$app->params['app.author'] ?> Service
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Web Development
                                </div>
                                <div class="margin-top-10 fs-14">
                                    iOS Development
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Android Development
                                </div>
                                <div class="margin-top-10 fs-14">
                                    Server Setup
                                </div>
                            </div>
                            <div class="box-3 m-margin-bottom-30">
                                <div class="text-gray">
                                    <?= Yii::$app->params['app.name'] ?> Everywhere
                                </div>
                                <div class="margin-y-15 fs-18 text-center">
                                    <i class="fa fa-facebook circle-icon bg-light-azure border-lightest margin-x-10"></i>
                                    <i class="fa fa-twitter circle-icon bg-light-azure border-lightest margin-x-10"></i>
                                    <i class="fa fa-instagram circle-icon bg-light-azure border-lightest margin-x-10"></i>
                                </div>
                                <div class="text-center">
                                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" style="width:80px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-darkest">
                    <div class="container padding-y-20 text-center">
                        <div class="margin-bottom-5">
                            <?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?>. All rights reserved. Use of this site is subject to certain Terms and Conditions.
                        </div>
                        <div>
                            <?= Yii::$app->params['app.name'] ?> - <?= Yii::$app->params['app.description'] ?>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- /END @FOOTER -->
        </section>
        <!-- /END @WRAPPER -->

        <!-- START @BACK TO TOP -->
        <!-- <div id="back-to-top">
            <div class="padding-10 bg-gray fs-18 border">
                <span class="glyphicon glyphicon-arrow-up"></span>
            </div>
        </div> -->
        <!-- /END @BACK TO TOP -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>