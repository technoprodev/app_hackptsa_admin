<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\Select2Asset::register($this);

$error = false;
$errorMessage = '';
if ($model['userIdentity']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['userIdentity'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>
    
<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['userIdentity'], 'password')->begin(); ?>
                <?= Html::activeLabel($model['userIdentity'], 'password', ['class' => 'control-label', 'label' => 'New Password']); ?>
                <?= Html::activePasswordInput($model['userIdentity'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
                <?= Html::error($model['userIdentity'], 'password', ['class' => 'help-block']); ?>
            <?= $form->field($model['userIdentity'], 'password')->end(); ?>
        </div>
        <div class="box-6 padding-right-0">
            <?= $form->field($model['userIdentity'], 'repassword')->begin(); ?>
                <?= Html::activeLabel($model['userIdentity'], 'repassword', ['class' => 'control-label']); ?>
                <?= Html::activePasswordInput($model['userIdentity'], 'repassword', ['class' => 'form-control', 'maxlength' => true]); ?>
                <?= Html::error($model['userIdentity'], 'repassword', ['class' => 'help-block']); ?>
            <?= $form->field($model['userIdentity'], 'repassword')->end(); ?>
        </div>
    </div>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>