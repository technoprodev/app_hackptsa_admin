<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['mediator']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['mediator'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['mediator'], 'nama')->begin(); ?>
        <?= Html::activeLabel($model['mediator'], 'nama', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['mediator'], 'nama', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['mediator'], 'nama', ['class' => 'help-block']); ?>
    <?= $form->field($model['mediator'], 'nama')->end(); ?>

    <?= $form->field($model['mediator'], 'id_departemen')->begin(); ?>
        <?= Html::activeLabel($model['mediator'], 'id_departemen', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['mediator'], 'id_departemen', ArrayHelper::map(\app_hackptsa_admin\models\Departemen::find()->indexBy('id')->asArray()->all(), 'id', 'departemen'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['mediator'], 'id_departemen', ['class' => 'help-block']); ?>
    <?= $form->field($model['mediator'], 'id_departemen')->end(); ?>

    <?= $form->field($model['mediator'], 'nomor_hp')->begin(); ?>
        <?= Html::activeLabel($model['mediator'], 'nomor_hp', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['mediator'], 'nomor_hp', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['mediator'], 'nomor_hp', ['class' => 'help-block']); ?>
    <?= $form->field($model['mediator'], 'nomor_hp')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['mediator']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>