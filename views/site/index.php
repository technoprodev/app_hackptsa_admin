<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="has-bg-img">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/cover.jpeg" class="bg-img" style="width: 100%; height: auto;">
    <!-- <img src="../technoart/asset/img/10-azure.png" class="bg-img" style="width: 100%; height: auto;"> -->
    <div class="container padding-y-200 m-padding-y-60 text-lightest">
        <div class="margin-bottom-50">
            <span class="padding-20 fs-40 m-fs-26 bg-red"><?= Yii::$app->params['app.name'] ?></span>
        </div>
        <div>
            <span class="padding-20 fs-20 m-fs-13 bg-red"><?= Yii::$app->params['app.description'] ?></span>
        </div>
    </div>
</div>