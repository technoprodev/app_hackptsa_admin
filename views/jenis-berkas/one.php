<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-6 margin-top-15">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['jenis-berkas']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['jenis-berkas']->id ? $model['jenis-berkas']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['jenis-berkas']->attributeLabels()['id_jenis_permintaan_dok'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['jenis-berkas']->id_jenis_permintaan_dok ? $model['jenis-berkas']->id_jenis_permintaan_dok : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['jenis-berkas']->attributeLabels()['nama_berkas'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['jenis-berkas']->nama_berkas ? $model['jenis-berkas']->nama_berkas : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['jenis-berkas']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['jenis-berkas']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>