<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['jenis-berkas']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['jenis-berkas'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['jenis-berkas'], 'id_jenis_permintaan_dok')->begin(); ?>
        <?= Html::activeLabel($model['jenis-berkas'], 'id_jenis_permintaan_dok', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['jenis-berkas'], 'id_jenis_permintaan_dok', ['class' => 'form-control']) ?>
        <?= Html::error($model['jenis-berkas'], 'id_jenis_permintaan_dok', ['class' => 'help-block']); ?>
    <?= $form->field($model['jenis-berkas'], 'id_jenis_permintaan_dok')->end(); ?>

    <?= $form->field($model['jenis-berkas'], 'nama_berkas')->begin(); ?>
        <?= Html::activeLabel($model['jenis-berkas'], 'nama_berkas', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['jenis-berkas'], 'nama_berkas', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['jenis-berkas'], 'nama_berkas', ['class' => 'help-block']); ?>
    <?= $form->field($model['jenis-berkas'], 'nama_berkas')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['jenis-berkas']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>