if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            jenis_permintaan_dok: {
                jenisBerkases: [],
            },
        },
        methods: {
            addJenisBerkas: function() {
                this.jenis_permintaan_dok.jenisBerkases.push({
                    id: null,
                    id_jenis_permintaan_dok: null,
                    nama_berkas: null
                });
            },
            removeJenisBerkas: function(i, isNew) {
                if (this.jenis_permintaan_dok.jenisBerkases[i].id == null)
                    this.jenis_permintaan_dok.jenisBerkases.splice(i, 1);
                else
                    this.jenis_permintaan_dok.jenisBerkases[i].id*=-1;
            },
        },
    });
}