<?php
namespace app_hackptsa_admin\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_hackptsa_admin\models\JenisPermintaanDok;
use app_hackptsa_admin\models\JenisBerkas;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * JenisPermintaanDokController implements highly advanced CRUD actions for JenisPermintaanDok model.
 */
class JenisPermintaanDokController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Jenis permintaan dok'], ['create', 'Create Jenis permintaan dok'], ['update', 'Update Jenis permintaan dok'], ['delete', 'Delete Jenis permintaan dok'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = JenisPermintaanDok::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('jenis_permintaan_dok');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'jenis_permintaan_dok',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'Daftar Permintaan Jenis Dokumen',
            ]);
        }
        
        // view single data
        $model['jenis_permintaan_dok'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Permintaan Jenis Dokumen ' . $model['jenis_permintaan_dok']->jenis_permintaan_dok,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = JenisPermintaanDok::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelJenisBerkas($id)
    {
        if (($model = JenisBerkas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['jenis_permintaan_dok'] = isset($id) ? $this->findModel($id) : new JenisPermintaanDok();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['jenis_permintaan_dok']->load($post);
            if (isset($post['JenisBerkas'])) {
                foreach ($post['JenisBerkas'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $jenisBerkas = $this->findModelJenisBerkas($value['id']);
                        $jenisBerkas->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $jenisBerkas = $this->findModelJenisBerkas(($value['id']*-1));
                        $jenisBerkas->isDeleted = true;
                    } else {
                        $jenisBerkas = new JenisBerkas();
                        $jenisBerkas->setAttributes($value);
                    }
                    $model['jenis_berkas'][] = $jenisBerkas;
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['jenis_permintaan_dok'])
                );
                return $this->json($result);
            }

            $transaction['jenis-permintaan-dok'] = JenisPermintaanDok::getDb()->beginTransaction();

            try {
                if ($model['jenis_permintaan_dok']->isNewRecord) {}
                if (!$model['jenis_permintaan_dok']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $error = false;
                if (isset($model['jenis_berkas']) and is_array($model['jenis_berkas'])) {
                    foreach ($model['jenis_berkas'] as $key => $jenisBerkas) {
                        if ($jenisBerkas->isDeleted) {
                            if (!$jenisBerkas->delete()) {
                                $error = true;
                            }
                        } else {
                            $jenisBerkas->id_jenis_permintaan_dok = $model['jenis_permintaan_dok']->id;
                            if (!$jenisBerkas->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['jenis-permintaan-dok']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['jenis-permintaan-dok']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['jenis-permintaan-dok']->rollBack();
            }
        } else {
            foreach ($model['jenis_permintaan_dok']->jenisBerkases as $key => $jenisBerkas)
                $model['jenis_berkas'][] = $jenisBerkas;

            if ($model['jenis_permintaan_dok']->isNewRecord) {
                $model['jenis_berkas'][] = new JenisBerkas();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Tambah Permintaan Jenis Dokumen',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['jenis_permintaan_dok']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['jenis_permintaan_dok'] = isset($id) ? $this->findModel($id) : new JenisPermintaanDok();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['jenis_permintaan_dok']->load($post);
            if (isset($post['JenisBerkas'])) {
                foreach ($post['JenisBerkas'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $jenisBerkas = $this->findModelJenisBerkas($value['id']);
                        $jenisBerkas->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $jenisBerkas = $this->findModelJenisBerkas(($value['id']*-1));
                        $jenisBerkas->isDeleted = true;
                    } else {
                        $jenisBerkas = new JenisBerkas();
                        $jenisBerkas->setAttributes($value);
                    }
                    $model['jenis_berkas'][] = $jenisBerkas;
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['jenis_permintaan_dok'])
                );
                return $this->json($result);
            }

            $transaction['jenis-permintaan-dok'] = JenisPermintaanDok::getDb()->beginTransaction();

            try {
                if ($model['jenis_permintaan_dok']->isNewRecord) {}
                if (!$model['jenis_permintaan_dok']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                $error = false;
                if (isset($model['jenis_berkas']) and is_array($model['jenis_berkas'])) {
                    foreach ($model['jenis_berkas'] as $key => $jenisBerkas) {
                        if ($jenisBerkas->isDeleted) {
                            if (!$jenisBerkas->delete()) {
                                $error = true;
                            }
                        } else {
                            $jenisBerkas->id_jenis_permintaan_dok = $model['jenis_permintaan_dok']->id;
                            if (!$jenisBerkas->save()) {
                                $error = true;
                            }
                        }
                    }
                }
                
                if ($error) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['jenis-permintaan-dok']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['jenis-permintaan-dok']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['jenis-permintaan-dok']->rollBack();
            }
        } else {
            foreach ($model['jenis_permintaan_dok']->jenisBerkases as $key => $jenisBerkas)
                $model['jenis_berkas'][] = $jenisBerkas;

            if ($model['jenis_permintaan_dok']->isNewRecord) {
                $model['jenis_berkas'][] = new JenisBerkas();
            }

            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Permintaan Jenis Dokumen ' . $model['jenis_permintaan_dok']->jenis_permintaan_dok,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['jenis_permintaan_dok']->id]);
    }

    /**
     * Deletes an existing JenisPermintaanDok model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
