<?php
namespace app_hackptsa_admin\controllers;

use Yii;
use app_hackptsa_admin\models\JenisBerkas;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * JenisBerkasController implements highly advanced CRUD actions for JenisBerkas model.
 */
class JenisBerkasController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Jenis berkas'], ['create', 'Create Jenis berkas'], ['update', 'Update Jenis berkas'], ['delete', 'Delete Jenis berkas'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = JenisBerkas::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('jenis_berkas');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'id_jenis_permintaan_dok',
                'nama_berkas',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Jenis berkas',
            ]);
        }
        
        // view single data
        $model['jenis-berkas'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Jenis berkas ' . $model['jenis-berkas']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['jenis-berkas'] = isset($id) ? $this->findModel($id) : new JenisBerkas();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['jenis-berkas']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['jenis-berkas'])
                );
                return $this->json($result);
            }

            $transaction['jenis-berkas'] = JenisBerkas::getDb()->beginTransaction();

            try {
                if ($model['jenis-berkas']->isNewRecord) {}
                if (!$model['jenis-berkas']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['jenis-berkas']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['jenis-berkas']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['jenis-berkas']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Jenis berkas',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['jenis-berkas']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['jenis-berkas'] = isset($id) ? $this->findModel($id) : new JenisBerkas();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['jenis-berkas']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['jenis-berkas'])
                );
                return $this->json($result);
            }

            $transaction['jenis-berkas'] = JenisBerkas::getDb()->beginTransaction();

            try {
                if ($model['jenis-berkas']->isNewRecord) {}
                if (!$model['jenis-berkas']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['jenis-berkas']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['jenis-berkas']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['jenis-berkas']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Jenis berkas ' . $model['jenis-berkas']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['jenis-berkas']->id]);
    }

    /**
     * Deletes an existing JenisBerkas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JenisBerkas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JenisBerkas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JenisBerkas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
