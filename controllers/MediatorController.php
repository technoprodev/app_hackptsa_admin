<?php
namespace app_hackptsa_admin\controllers;

use Yii;
use app_hackptsa_admin\models\Mediator;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * MediatorController implements highly advanced CRUD actions for Mediator model.
 */
class MediatorController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Mediator'], ['create', 'Create Mediator'], ['update', 'Update Mediator'], ['delete', 'Delete Mediator'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = Mediator::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('mediator m')
                ->join('LEFT JOIN', 'departemen d', 'd.id = m.id_departemen')
                ;
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'm.id',
                'm.nama',
                'd.departemen AS departemen',
                'm.nomor_hp',
                'm.created_at',
                'm.updated_at',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Mediators',
            ]);
        }
        
        // view single data
        $model['mediator'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Mediator ' . $model['mediator']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['mediator'] = isset($id) ? $this->findModel($id) : new Mediator();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['mediator']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['mediator'])
                );
                return $this->json($result);
            }

            $transaction['mediator'] = Mediator::getDb()->beginTransaction();

            try {
                if ($model['mediator']->isNewRecord) {}
                if (!$model['mediator']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['mediator']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['mediator']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['mediator']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Mediator',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['mediator']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['mediator'] = isset($id) ? $this->findModel($id) : new Mediator();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['mediator']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['mediator'])
                );
                return $this->json($result);
            }

            $transaction['mediator'] = Mediator::getDb()->beginTransaction();

            try {
                if ($model['mediator']->isNewRecord) {}
                if (!$model['mediator']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['mediator']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['mediator']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['mediator']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Mediator ' . $model['mediator']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['mediator']->id]);
    }

    /**
     * Deletes an existing Mediator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mediator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mediator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mediator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
